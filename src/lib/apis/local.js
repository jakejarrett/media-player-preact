import { app } from "electron";
import recursive from "recursive-readdir";
import homedir from "os-homedir";
import MusicMetadata from "musicmetadata";
import firstBy from 'thenby'
import fs from "fs";

class Sort {

	static album (first, second) {
		return first.album.localeCompare(second.album);
	};

	static artist (first, second) {
		return first.artist.localeCompare(second.artist);
	};

	static tracks (first, second) {
		return first.track.no - second.track.no;
	};

	static title (first, second) {
		return first.title.localeCompare(second.title);
	};

	static firstRun (first, second) {
		const artistComparison = this.artist(first, second);
		const albumComparison = this.album(first, second);
		let comparator = artistComparison;

		/** If they're the same album, sort by the track number */
		if (0 === artistComparison && 0 === albumComparison) {
			comparator = albumComparison
		}
		
		/** Otherwise sort by their title */
		if (0 === artistComparison && 0 !== albumComparison) {
			comparator = artistComparison;
		}
		
		return comparator;
	}

	static secondRun (first, second) {
		const artistComparison = this.artist(first, second);
		const albumComparison = this.album(first, second);
		let comparator = artistComparison;
		
		/** Otherwise sort by their title */
		if (first.title === second.title && 0 !== albumComparison) {
			comparator = this.title(first, second);
		}

		
		/** If they're the same album, sort by the track number */
		if (0 === albumComparison || first.album === second.album) {
			comparator = this.tracks(first, second);
		}

		return comparator;
	}
}

class Local {

    __songs = [];

	/**
	 * API service constructor
	 *
	 * @returns {Array} The list of songs
	 */
	constructor (store) {
		const that = this;
		let userHome = homedir();

		/**
		 * TODO- Make this dynamic
		 */
		const filesDone = new Promise((resolve, reject) => {
			let count = 0;
			let songs = [];

			const check = files => {
				if (count === files.length) {
					resolve({ songs: this.__songs });
				}
			};

			recursive(`${userHome}/Music`, (err, files) => {
				files.forEach(file => {
					const fileStream = fs.createReadStream(file);

					MusicMetadata(fileStream, { duration: true }, (err, metadata) => {
                        const splitFile = file.split(`/`);
						let artwork = `data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAfQAAAH0CAMAAAD8CC+4AAABNVBMVEWns9aptdfY3e28xeCuudmwu9qotNfT2evFzeTW3OzZ3u3L0ue2wN3V2+ytuNnW2+yst9jU2uvJ0ObR1+rX3O2qtdfEzOS3wd69xuDCyuPGzuXByeLAyeKxvNvM0+eqttjX3e24wt6/yOGzvty6w9+qttfP1um7xN/Q1+nK0eart9i4wd61v92ptNfY3u2yvdvP1emzvdy3wN2xu9u0v9y1v9zGzeTQ1unHz+XO1OjJ0eawutrO1ejO1emyvNvHzuXByuLV2uvAyOLIz+a9xuHK0ue7xOCsuNnK0efN0+jU2evM0+jR2OrIz+Wtudm+xuG3wd26xN/Cy+O5w9+zvdvDzOPEzOO7xeC5w96/x+Ha3+7S2OrX3OyotNavutq5wt7Z3u6rttjN1OjI0Oa+x+G0vtzDy+NWNiRqAAAH5ElEQVR4AezBgQAAAACAoP2pF6kCAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAGbnvpbbVpIwjjcCMykGKuecc5CVnbN9ctj9KNCgJOr9H2Hv5mLL6yOAwAKY+X4vgIt/qUus6WqzeP3HjpBBet/ubwEImaL7cOcDYHSDhnoBipD2er+3hgEwukFD/dwHYEp0yh+PFqDoH516y2qoGxGdLhevfACmRKf8iRrqRkSnzeWLeQDGRKfc4j4URtdffrv4CYAx0WlzbcMGYEx0yr1ahcLo+tvZLjYBGBOdGmqomxGdjqZXoTC6/naqaqgzuhEa797aABjdGDPTz6Awuv6s6ulLAIxujPdOxQbA6Ma4mVJDndFNYP0yWQPA6MYYciq7ABjdGDcv1qEwuv6sMTXUGd2QoT67C4DRjbE30YbC6Pqztp7XATC6MX4any0DYHRTeH+ooc7oRhjZOqsDYHRjuOOfywAY3RTewUQHiv7RaWXhrATAmOjkvvnoAzAlOnlLjx0o+kenuYXXJQDGRCdXnXcyJjohOYzO6MToxOjE6MToxOjE6MTohAT55w9dk6Iz+nxruScixkRn9E+jJ3kRMSY6o68u5kRhdP2j22/XGqIwuv7Ra6dVSxRG1z/6+tSMKIyuf/TdWWdIFEbXP3r9+ZglCqPrH70zceCJwujaR/fv3riiMLr+0W/vv82Jwuj6Ry889j1RGF376P7VYlcURtc/un2xvCkKo+sfvVnczovC6PpHX311JAqj6x/drrxriMLo+kevTR5aojC6/tHbL25EYXT9o5fVK4oZ0Rm9dLY1Igqj6x+986heURjdhOj+3YMriv7RGX249a+eKPpHZ/TC6LEniv7RGf168VIU/aMzur2xtimK/tEZvVms7oiif3RGfzY9I4r+0Rl9t+K8F0X/6Ixen1Rry4xuRPT2xJ4o+kdn9PLncVcU/aMzeunrwooo+kdn9I5aW2Z0Y6In/n1idGJ0YnRidGJ0YnRidGL0BDE6MToxOjE6MToxOjE6MToxeoIYnRidGJ0YnRidGJ0YnRidGD1BjE6MToxOjE6MToxOjE6MToyeIEYnRidGJ0YnRidGJ0YnRidGTxCjE6MToxOjE6MToxOjE6MToyeI0YnRidGJ0YnRidETxOhzl9u/Lr6YKJ6e/vbK+WVmRO/ojH65Vjwv4b+8rEz381pGZ/Tcm40S/pfh1u8rmkVn9IPRJv6BPXmkT3RG/2mqiSe5WtIjOqMvVfB0lVz2ozP64T4C8ae9bEdn9P4qAlvvZjg6o7sbCGO+mtXojJ6fKiMcfzyb0Rn9qI0ISIYw+ngZjG5W9JEWwOhmRR/6AkY3LPpMDYxuWPRjG4xuWPRtG4xuWPS+DUY3LHpuGIxuWPTNJhjdsOjeHRjdtOjTYHTTov/hM7pp0XcKYHTTok+D0U2L7u4yunHRv4LRTYvu+oxuXPRJMLpp0XtlxMATSnH0nxGHFaEUR3+GOIwIpTf6EGIhlOLoDuIwLJTi6K8Rh4JQiqMXEIdZofRGtxCLR0kvRs8hFguSXox+jFj0hKKTn/MijT6GOKzL4Gio+lCsrNdsAMBt4er11LduJNF/RRymZDCUG2/V8B31t4tHA0dfRhxcofC84x+f9CpM3AwWvYoYXAuFlvuzhH/UXtsZIPoSYrAsFE5+eR9PU3oxFzq6i+i9zAuFYT3U8HS3r/Iho3s2IucIheA5JQRT6IeLLteIWiEvFNxYB8GNWqGijyJqVaHAhioIpZMLE30MEasIBebMI4TQx/ssG5EqNYQC6lUQSvjjffeI1JhQQHtNRCPAJxGlolBAjp/Ajto+onOXFwrmEUgg+gkiU9gUCsR7jUSiSwURqblCgXgXSCi6ayMS9a5QIN49koouDqJQywkF8xeSiy4fMLiOKxSMgySjr7QxqOueUDB9P9HoMtTEYEbzQsE06kg2ugwVMAB7QSioWSQdXd63EdqdKxSUg+Sji7WBcOYdocB6pUSjK/8uI4SvDaHgJpGO6JJbR1AfZ4RCyCEG/581rQ97QqHcpyC6Mjf15Oz25KVQOF3EQUKzfu7gCfbH54TCKqYmurL0vIQf8c8fXKHwrHmk8I6bt/Tbdfn7wVcfD+dkILSAtN5xsw7+Hv3cuVW1a6sfJpZnLBkYtZDyO247Pdedcd2eUFS8Yd5xM84NeMfNOA5i8UVMwx9saEl60R149MU4BcSiKulFw4hD2ZL0IsTio6QYlQHwAIhh6oiBvSIpRh3E4EzSjPYRg5ykGZ0heheSavTAP3Tz7GFgvADCp9XaipBpu7BbknZU5XA30CdEqW2JYfj/e90VyoCRGiJj94Uy4R2iUj4UygZvnc3NkysjCrtVoewYRwTqB0KG7cR+cYUyxatgQK0RoYyxZjEIe00oe/IbCO+qK5RJEwjp9m+hrNoqIQT/tCGUXY0KArvoCmXbVhNB+PdHQplnLdbxVKU/h4S0YDkFPIFfWcgL6WPp+Tx+qDzrNIQ0k98udvB9/rO/xkaE9NQ7nG61bSjwX54XnT6D68/qLp1UF8ZO+jkO9P+0B8cCAAAAAIP8raexo1oBAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAACAtyPI7eLNzsAAAAABJRU5ErkJggg==`;
						const durationAsSeconds = metadata.duration;

						if (err) {
							reject(err);
						}

						if (metadata.picture.length > 0) {
							let picture = metadata.picture[0];
							artwork = URL.createObjectURL(new Blob(
								[picture.data], {
									'type': `image/${picture.format}`
								}
							));
						}

						that.__songs.push({
							src: file,
							artist: metadata.artist[0] || "Unknown artist",
							title: metadata.title || splitFile[splitFile.length - 1],
							album: metadata.album || "",
							albumArtist: metadata.albumArtist || "",
							genre: metadata.genre[0] || "",
							artwork: artwork,
							id: count,
							duration: durationAsSeconds,
                            identifier: parseInt(Math.random()*10000),
							track: metadata.track
						});
						count++;
						fileStream.close();
					});

					window.requestIdleCallback(_ => check(files));
				});

			})

		});
		filesDone.catch(e => {
			console.log(e);
		});

		filesDone.then(_ => {
			const songs = that.__songs.splice(0);

			songs.sort(
				firstBy((first, second) => Sort.firstRun(first, second))
				.thenBy((first, second) => Sort.secondRun(first, second))
			)

			store.dispatch({ type: 'ADD_SONGS', API: 'LOCAL', SONGS: songs });

			return this.__songs;
		});
	}

	get songs () {
		return this.__songs;
	}

	get title () {
		return "Local";
	}

}

export default Local;

import { h, Component } from 'preact';
import { Router } from 'preact-router';
import { createStore } from 'redux';
import reducers from './reducers';

import Player from './components/player';
import Home from './views/home';
import Profile from './views/profile';

const store = createStore(reducers);

export default class App extends Component {
	/** Gets fired when the route changes.
	 *	@param {Object} event		"change" event from [preact-router](http://git.io/preact-router)
	 *	@param {string} event.url	The newly routed URL
	 */
	handleRoute = e => {
		this.currentUrl = e.url;
	};

	render() {
		return (
			<div id="app">
				<Router onChange={this.handleRoute}>
					<Home path="/" store={store} />
					<Profile path="/profile/" store={store} user="me" />
					<Profile path="/profile/:user" store={store} />
				</Router>
				<Player store={store} />
			</div>
		);
	}
}

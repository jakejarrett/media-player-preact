import { h, Component } from 'preact';
import Song from '../../components/song';
import API from '../../lib/api-facade';
import style from './style.less';

export default class Home extends Component {

	constructor (options) {
		super(options);
		this.APIFacade = new API();
		this.APIFacade.initialize({ store: this.props.store });
		this.props.store.subscribe(this.onStateChange.bind(this));
	}

	onStateChange () {
		this.setState(this.props.store.getState());
	}

	render ({ store }, { SONGS }) {
		SONGS = SONGS || [];
		
		return (
			<div class={style.homepage}>
				{SONGS.map(song => (<Song artist={song.artist} artwork={song.artwork} title={song.title} src={song.src} track={song.track.no} store={store} identifier={song.identifier} album={song.album} />))}
			</div>
		);
	}
}

export default (state = { API: 'none', SONGS: [], PLAYING: { identifier: false }, STATUS: 'paused' }, action) => {
  switch (action.type) {
    case 'ADD_SONGS': {
        return { API: action.API, SONGS: action.SONGS, PLAYING: action.PLAYING || { identifier: false } };
    }

    case 'PLAYING': {
        return { API: action.API, SONGS: action.SONGS, PLAYING: action.PLAYING, STATUS: action.STATUS };
    }

    case 'PAUSE': {
        console.log(action.STATUS)
        return { API: action.API, SONGS: action.SONGS, PLAYING: action.PLAYING, STATUS: action.STATUS };
    }

    default: {
        return { API: 'none', SONGS: [], PLAYING: { identifier: false } };
    }
  }
}
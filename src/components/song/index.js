import { h, Component } from 'preact';
import style from './style.less';

export default class Song extends Component {

    songState = 'paused';

    determineIcon () {
        let icon = 'icon-control-play';
        const state = this.props.store.getState();

        if (state.PLAYING.identifier === this.props.identifier && state.STATUS === 'playing') {
            icon = 'icon-control-pause';
        }

        return icon;
    }

    onClick (event) {
        let currentState = this.props.store.getState();
        const { artist, artwork, title, identifier, src } = this.props;

        if (this.songState === 'paused' || currentState.PLAYING.identifier !== this.props.identifier && currentState.STATUS === 'playing') {
            currentState.type = 'PLAYING';
            this.songState = 'playing';
        } else {
            currentState.type = 'PAUSE';
            this.songState = 'paused';
        }

        currentState.STATUS = this.songState;
        currentState.PLAYING = { artist, artwork, title, identifier, src };

        this.props.store.dispatch(Object.assign({}, currentState));
    }

	render ({ album, artist, artwork, title, track, identifier }, { PLAYING }) {
        const songBG = `background-image: url(${artwork})`;
        const that = this;
		return (
            <div className={style.song} style={songBG} onClick={this.onClick.bind(that)}>
	            <div class={style.hover} style={this.determineStateHover.call(that)}><i class={this.determineIcon.call(that)}></i></div>
	            <p style={this.determineStatePTag.call(that)}>
		            <span>{title}</span> <br />
		            <span>{artist}</span>
	            </p>
            </div>
		);
	}

    determineStateHover () {
        let returnValue;
        const state = this.props.store.getState();

        if (state.PLAYING.identifier === this.props.identifier && state.STATUS === 'playing') {
            returnValue = 'display:flex;';
        }

        return returnValue;
    }

    determineStatePTag () {
        let returnValue;
        const state = this.props.store.getState();

        if (state.PLAYING.identifier === this.props.identifier && state.STATUS === 'playing') {
            returnValue = 'background:transparent;';
        }

        return returnValue;
    }

}

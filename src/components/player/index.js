import { h, Component } from 'preact';
import style from './style.less';

export default class Player extends Component {

    constructor (...args) {
        super(...args);

        this.props.store.subscribe(this.onStateChange.bind(this));
    }

    onStateChange () {
        const that = this;
        const state = this.props.store.getState();
        const player = document.querySelector('#audio-player');

        if (state.PLAYING.identifier !== false) {
            this.setState({
                artist: state.PLAYING.artist,
                artwork: state.PLAYING.artwork,
                title: state.PLAYING.title,
                src: `file://${state.PLAYING.src}`
            });
        }

        if (state.PLAYING.identifier !== false && state.STATUS === 'paused') {
            player.pause()
        } 
        
        if (state.PLAYING.identifier !== false && state.STATUS === 'playing') {
            player.play();
        }
    }

    /**
     * When the player emits a progress event
     * 
     * @param {Event} e Progress event
     */
    onPlayerProgress (e) {
        console.log(e);
    }

    determineStyle () {
        const playing = this.props.store.getState().PLAYING.identifier !== false;
        let style = '';

        if (!playing) {
            style =  'display: none;'
        }

        return style;
    }

	render ({}, { artist, artwork, title, src }) {
        const that = this;

		return (
			<div class={style.player}>
                <div class={style.playerSlider}></div>
                <img class={style.artwork} src={artwork} />
                <div class={style.details}>
                    <span>{title}</span>
                    <span>{artist}</span>
                </div>
                <audio src={src} style="display: none;" autoplay="true" id="audio-player" onProgress={this.onPlayerProgress.bind(that)} />
                <div class={style.controls} style={this.determineStyle.call(that)}>
                    <span class='icon-control-start'></span>
                    <span class='icon-control-end'></span>
                </div>
			</div>
		);
	}
}
